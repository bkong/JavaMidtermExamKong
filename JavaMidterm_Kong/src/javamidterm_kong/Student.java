/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamidterm_kong;

/**
 *
 * @author student
 */
public class Student {

    private String idNumber;
    private String firstName;
    private String  lastName;
  
   

    public Student() {
    }

    public Student(String idNumber, String firstName, String lastName) {
        this.idNumber = idNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    
    }

    
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


   

   
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("ID Number  : %-20s", idNumber)).append("\n");
        sb.append(String.format("First Name : %-20s", firstName)).append("\n");
        sb.append(String.format("Last Name  : %-20s", lastName)).append("\n");    

        return sb.toString();
    }

}
