/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamidterm_kong;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author student
 */
public class Subject {
    
    private HashSet<Student> studList = new HashSet<>();
    private HashMap<String,Student> newStudList =  new HashMap<>();

    public Subject() {
        
    }
    public HashSet<Student> getStudList() {
        return studList;
    }

    public void setStudList(HashSet<Student> studList) {
        this.studList = studList;
    }

    public HashMap<String, Student> getNewStudList() {
        return newStudList;
    }

    public void setNewStudList(HashMap<String, Student> newStudList) {
        this.newStudList = newStudList;
    }

    public void addStudent(Student a){
        studList.add(a);
    }    
    
    public void addStudentToHashMap(){
            
        for(Student a : studList){
            newStudList.put(a.getIdNumber(), a);
        }
    }

    
    
    
    
    
    
}
