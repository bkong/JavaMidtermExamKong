/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamidterm_kong;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author student
 */
public class JavaMidterm_Kong {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        ArrayList<Student> arrayStudList = new ArrayList<>();
        arrayStudList.add(new Student("1", "Bernard", "Kong"));
        arrayStudList.add(new Student("2", "Randolf", "Lapiña"));
        arrayStudList.add(new Student("3", "Angelie", "Escaran"));
        arrayStudList.add(new Student("4", "Rinalyn", "Bonajos"));
        arrayStudList.add(new Student("5", "Annie", "Manzano"));

        LinkedList<Student> linkedStudent = new LinkedList<>();

        ArrayList<String> names = new ArrayList<>();

        Subject list = new Subject();

        System.out.println("Before Sorting");
        for (Student a : arrayStudList) {
            System.out.println(a.toString());
        }

        for (Student s : arrayStudList) {
            names.add(s.getLastName());
        }

        Collections.sort(names);

        for (String a : names) {
            for (Student b : arrayStudList) {
                if (a.equals(b.getLastName())) {
                    linkedStudent.add(b);
                }
            }
        }

        System.out.println("After Sorting");

        for (Student a : linkedStudent) {
            System.out.println(a.toString());
        }

        for(Student a : linkedStudent){
            list.addStudent(a);
        }
        list.addStudentToHashMap();
        
        System.out.println("Display in HashMap");
        
        for(Map.Entry m : list.getNewStudList().entrySet()){
            System.out.println(m.getValue());
        }
        

    }

}
